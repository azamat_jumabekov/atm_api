require 'grape'
require 'grape-swagger'

Dir["#{File.dirname(__FILE__)}/app/api/**/*.rb"].each { |f| require f }

module API
  class Root < Grape::API
    format :json

    mount ATM::Base => '/api'
    add_swagger_documentation
  end
end

Application = Rack::Builder.new do
  map "/" do
    run API::Root
  end
end

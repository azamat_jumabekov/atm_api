require_relative './atm/withdraw'
require_relative './atm/replenish'
require_relative './atm/constants'
require_relative './memory'

module ATM
  class Logic
    def self.init(initial_values: nil)
      self.new(initial_values: initial_values)
    end

    def initialize(storage: Memory.storage, initial_values: {})
      storage[:banknotes] = {} if storage[:banknotes].nil?
      @banknote_amounts = storage[:banknotes]
      setup(initial_values) if storage[:banknotes].empty?
    end

    def replenish(payload)
      ATM::Replenish.new(@banknote_amounts, payload).call
    end

    def withdraw(withdraw_amount)
      ATM::Withdraw.new(@banknote_amounts, withdraw_amount).call
    end

    def total
      Memory.storage[:banknotes]
    end

    private

    def setup(values)
      ATM::Constants::BANKNOTES.each do |banknote|
        @banknote_amounts[banknote] = values.fetch(banknote, ATM::Constants::BANKNOTE_INITIAL_AMOUNT)
      end
    end
  end
end

require_relative './constants'
require_relative './errors'

module ATM
  class Withdraw
    def initialize(amounts, withdraw_amount)
      @withdraw_amount = withdraw_amount
      @amounts = amounts
    end

    def call
      return unless @withdraw_amount.is_a?(Integer)

      @possible_banknotes = calculate_possible_banknotes
      raise InsufficientFundsError if insufficient_funds

      @possible_banknotes.each do |banknote, amount|
        @amounts[banknote] ||= 0
        @amounts[banknote] -= amount
      end
    end

    private

    def calculate_possible_banknotes
      possible = {}
      amount = @withdraw_amount
      reversed_banknotes = ATM::Constants::BANKNOTES.sort {|a, b| b <=> a}
      reversed_banknotes.each do |banknote|
        next if @amounts[banknote].to_i <= 0
        next if @amounts[banknote].to_i < possible[banknote].to_i

        loop do
          break if @amounts[banknote].to_i <= possible[banknote].to_i

          remainder = 0
          remainder = amount - banknote
          break if remainder < 0

          amount -= banknote
          possible[banknote] ||= 0
          possible[banknote] += 1
          break if amount <= 0
        end
      end
      possible
    end

    def insufficient_funds
      possible_sum = @possible_banknotes.inject(0) {|a, b| a + (b[0] * b[1]) }
      possible_sum < @withdraw_amount
    end
  end
end

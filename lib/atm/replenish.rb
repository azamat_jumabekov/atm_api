require_relative './constants'

module ATM
  class Replenish
    def initialize(amounts, payload)
      @payload = filter_payload(payload.transform_keys { |k| k.to_s })
      @amounts = amounts
    end

    def call
      @payload.each do |k, v|
        @amounts[k] += v
      end
    end

    private

    def filter_payload(payload)
      filtered = {}

      ATM::Constants::BANKNOTES.each do |banknote|
        next unless payload[banknote.to_s].is_a?(Integer)

        filtered[banknote] = payload[banknote.to_s]
      end

      filtered
    end
  end
end

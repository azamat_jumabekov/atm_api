module ATM
  module Constants
    BANKNOTES = [1, 2, 5, 10, 25, 50].freeze
    BANKNOTE_INITIAL_AMOUNT = 10.freeze
  end
end

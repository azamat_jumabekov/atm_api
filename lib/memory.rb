require 'singleton'

module Memory
  def self.storage
    DBInstance.instance.storage = {} if DBInstance.instance.storage.nil?
    DBInstance.instance.storage
  end

  class DBInstance
    include Singleton

    attr_accessor :storage
  end

  private_constant :DBInstance
end

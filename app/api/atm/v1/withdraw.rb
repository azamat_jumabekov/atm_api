require_relative '../../../../lib/atm'

module ATM
  module V1
    class Withdraw < Grape::API
      format :json

      namespace :v1 do
        params do
          ATM::Constants::BANKNOTES.each do |banknote|
            requires :amount, type: Integer
          end
        end
        post ':withdraw/:amount'do
          if ATM::Logic.new.withdraw(params[:amount])
            status 200
          else
            error!({ messages: 'Something went wrong' }, 422)
          end
        rescue InsufficientFundsError
          error!({ messages: 'Insufficient funds' }, 422)
        end
      end
    end
  end
end

require_relative '../../../../lib/atm'
require_relative '../../../../lib/memory'

module ATM
  module V1
    class CashBalances < Grape::API
      format :json

      namespace :v1 do
        get :cash_balances do
          ATM::Logic.new.total
        end
      end
    end
  end
end

require_relative '../../../../lib/atm'

module ATM
  module V1
    class Replenish < Grape::API
      format :json

      namespace :v1 do
        params do
          ATM::Constants::BANKNOTES.each do |banknote|
            optional banknote.to_s.to_sym, type: Integer
          end
        end
        post :replenish do
          if ATM::Logic.new.replenish(params)
            status 200
          else
            error!({ messages: 'Something went wrong' }, 422)
          end
        end
      end
    end
  end
end

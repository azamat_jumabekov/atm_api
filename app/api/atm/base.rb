require_relative 'v1/replenish'
require_relative 'v1/withdraw'
require_relative 'v1/cash_balances'

module ATM
  class Base < Grape::API
    mount ATM::V1::Replenish => '/atm'
    mount ATM::V1::Withdraw => '/atm'
    mount ATM::V1::CashBalances => '/atm'
  end
end

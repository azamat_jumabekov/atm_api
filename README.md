### Requirements

- RVM, Ruby 2.7.1

### Install

- bundle install

### Testing

`bundle exec rspec`

### Running the application

`rackup`

### Swagger docs

Run the application, visit `/swagger_doc`

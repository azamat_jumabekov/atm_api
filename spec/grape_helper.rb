require_relative '../application'
require "rack/test"

def app
  Application
end

require_relative 'support/memory_cleaner'

RSpec.configure do |config|
  config.include Rack::Test::Methods
end

require_relative '../../lib/atm'

RSpec.describe ATM do
  let(:params) { {} }
  let(:subject) { ATM::Logic.new(**params) }

  before(:each) do
    Memory.storage[:banknotes] = nil
  end

  context 'success' do
    describe 'with default initial values' do
      let(:expected) { {1=>10, 2=>10, 5=>10, 10=>10, 25=>10, 50=>10} }

      it 'has expected banknote amounts' do
        expect(subject.total).to eq(expected)
      end
    end

    describe 'with given initial values' do
      let(:initial_values) { {1=>12, 2=>32, 5=>34, 10=>54, 25=>32, 50=>76} }
      let(:params) { { initial_values: initial_values} }

      it 'has expected banknote amounts' do
        expect(subject.total).to eq(initial_values)
      end
    end

    describe 'replenish' do
      let(:replenish_payload) { {5=>4, 10=>7, 25=>3, 50=>8} }
      let(:expected) { {1=>10, 2=>10, 5=>14, 10=>17, 25=>13, 50=>18} }

      before do
        subject.replenish(replenish_payload)
      end

      it do
        expect(subject.total).to eq(expected)
      end
    end

    describe 'withdraw' do
      let(:withdraw_amount) { 85 }
      let(:expected) { {1=>10, 2=>10, 5=>10, 10=>9, 25=>9, 50=>9} }

      before do
        subject.withdraw(withdraw_amount)
      end

      it do
        expect(subject.total).to eq(expected)
      end
    end
  end

  context 'fail' do
    describe 'withdraw' do
      let(:withdraw_amount) { 85 }
      let(:expected) { {1=>2, 2=>3, 5=>3, 10=>0, 25=>0, 50=>0} }
      let(:params) { { initial_values: expected} }

      before do
        Memory.storage[:banknotes] = nil
      end

      it do
        expect{subject.withdraw(withdraw_amount)}.to raise_error(ATM::InsufficientFundsError)
      end
    end
  end
end

require_relative '../../../grape_helper'

describe '/api' do
  describe '/replenish' do
    context 'valid params' do
      let(:replenish_payload) { {5=>4, 10=>7, 25=>3, 50=>8} }
      let(:expected) { {1=>10, 2=>10, 5=>14, 10=>17, 25=>13, 50=>18} }

      it 'returns expected result' do
        post '/api/atm/v1/replenish/', replenish_payload
        expect(last_response.status).to eq(200)
        expect(Memory.storage[:banknotes]).to eq(expected)
      end
    end
  end
end

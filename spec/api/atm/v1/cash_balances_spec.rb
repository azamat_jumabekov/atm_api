require_relative '../../../grape_helper'

describe '/api' do
  describe '/cash_balances' do
    let(:expected) { {1=>10, 2=>10, 5=>10, 10=>10, 25=>10, 50=>10} }

    it 'returns expected result' do
      get '/api/atm/v1/cash_balances'
      result = JSON.parse(last_response.body)
      expected.each do |banknote, amount|
        expect(result[banknote.to_s]).to eq(expected[banknote])
      end
    end
  end
end

require_relative '../../../grape_helper'

describe '/api' do
  describe '/withdraw' do
    context 'valid params' do
      let(:withdraw_amount) { 85 }
      let(:expected) { {1=>10, 2=>10, 5=>10, 10=>9, 25=>9, 50=>9} }

      it 'returns expected result' do
        post '/api/atm/v1/withdraw/' + withdraw_amount.to_s
        expect(last_response.status).to eq(200)
        expect(Memory.storage[:banknotes]).to eq(expected)
      end
    end

    context 'invalid params' do
      let(:withdraw_amount) { 85 }
      let(:expected) { {1=>2, 2=>3, 5=>3, 10=>0, 25=>0, 50=>0} }

      before do
        ATM::Logic.new(initial_values: expected)
      end

      it 'fails' do
        post '/api/atm/v1/withdraw/' + withdraw_amount.to_s
        expect(last_response.status).to eq(422)
        expect(JSON.parse(last_response.body)).to eq({ 'messages'=>'Insufficient funds' })
        expect(Memory.storage[:banknotes]).to eq(expected)
      end
    end
  end
end
